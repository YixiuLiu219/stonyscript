# Yixiu Liu 

import ply.lex as lex
from Nodes import *

# tokenize
keywords = {
    'if': 'IF',
    'else if': 'ELIF',
    'else': 'ELSE',
    'while': 'WHILE',
    'in': 'IN',
    'not': 'NOT',
    'and': 'AND',
    'or': 'OR',
    'print' : 'PRINT',
    'return' : 'RETURN'
}
tokens = ['INTEGER', 'REAL', 'STRING', 'VAR', # LIST not in account, it's middle can be an expression
          'ASSIGN', 'SEMICOLON',
          'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'MOD', 'FLOOR', 'POWER',
          'LST', 'LEQ', 'GRT', 'GEQ', 'NEQ', 'EQV',
          'LPAREN', 'RPAREN', 'LBRACK', 'RBRACK', 'LBLOCK', 'RBLOCK', 'COMMA'] + list(keywords.values())

t_PLUS = r'\+'
t_MINUS = r'-'
t_POWER = r'\*\*'
t_TIMES = r'\*'
t_FLOOR = r'//'
t_DIVIDE = r'/'
t_MOD = r'%'
t_NEQ = r'<>'
t_EQV = r'=='
t_ASSIGN = r'='
t_LEQ = r'<='
t_LST = r'<'
t_GEQ = r'>='
t_GRT = r'>'
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_LBRACK = r'\['
t_RBRACK = r'\]'
t_LBLOCK = r'{'
t_RBLOCK = r'}'
t_COMMA = r'\,'
t_SEMICOLON = r';'


def t_VAR(t):
    r'[a-zA-Z_][a-zA-Z0-9_]*'
    t.type = keywords.get(t.value, 'VAR')
    return t


def t_STRING(t):        # should we include the ' ???
    r'\"[^\\\n\'\"]*\"'
    t.value = t.value[1:-1]
    return t


def t_REAL(t):
    r'(\d+\.\d*)|(\d*\.\d+)'
    try:
        t.value = float(t.value)
    except ValueError:
        print('Real value too large %f', t.value)
        t.value = 0
    return t


def t_INTEGER(t):
    r'\d+'
    try:
        t.value = int(t.value)
    except ValueError:
        print('Integer value too large %d', t.value)
        t.value = 0
    return t

t_ignore = ' \t\n'
'''
def t_newline(t):
    r'\n+'
    t.lexerlineno += t.value.count('\n')
'''
def t_error(t): #illegal token / char check
    #print("SYNTAX ERROR")
    #print('Illegal char \'%s\'' % t.value[0])
    t.lexer.skip(1)
    return t


lex.lex()





import ply.yacc as yacc

# parse
names = {}
precedence = (
               ('right', 'ASSIGN'),
               ('left', 'OR'),
               ('left', 'AND'),
               ('right', 'NOT'),
               ('left', 'LST', 'LEQ', 'GRT', 'GEQ', 'EQV', 'NEQ'),
               ('left', 'IN'),
               ('left', 'PLUS', 'MINUS'),
               ('left', 'FLOOR'),
               ('left', 'POWER'),
               ('left', 'MOD'),
               ('left', 'TIMES', 'DIVIDE'),
               ('nonassoc', 'LBRACK', 'RBRACK')
               )
'''
def p_statement_empty(t):
    'program :'
    pass
'''

# error when cannot recognize token
def p_statement_error(t):
    'program : error'
    raise ScriptSyntaxError("UNRECOGNIZED TOKEN: " + str(t))

# error when cannot recognize parse rule
def p_error(t):
    raise ScriptSyntaxError("FROM p_error, NO SUCH GRAMMAR: " + str(t))




# basic

def p_program(t):
    'program : outerstmt_head'
    t[0] = StatementList(t[1])


def p_outerstmt(t):
    '''outerstmt_head : function_declare outerstmt_head
    | main'''
    if len(t) == 3:
        t[0] = StatementHead(t[1], t[2])
    else:
        t[0] = Statement(t[1])



def p_main(t):
    'main : LBLOCK statement_head RBLOCK'
    # StatementList(t[2]).exec()    FOR TESTING, IT WILL EXEC UNTIL ERROR
    t[0] = StatementList(t[2])


def p_statement_block(t):
    '''statement : LBLOCK statement_head RBLOCK'''
    #t[0] = BlockNode(StatementList(t[2]))
    t[0] = StatementList(t[2])


def p_statement_list_head(t):
    '''statement_head : statement statement_head
    |'''
    if len(t) == 3:
        t[0] = StatementHead(t[1], t[2])
    else:
        t[0] = StatementHead()




def p_statement_return(t):                                      # RETURN
    'statement : RETURN expression SEMICOLON'
    t[0] = ReturnNode(t[2])


def p_statement_function_declare(t):                            # DECLARE
    'function_declare : VAR paramlist LBLOCK statement_head RBLOCK'
    t[0] = FunctionDeclareNode(ID(t[1]), t[2], t[4])

def p_paramlist(t):
    'paramlist : LPAREN param_head RPAREN'
    t[0] = t[2]
def p_paramlist_head(t):
    '''param_head : VAR param_mid
    |'''
    if len(t) == 3:
        t[0] = ListNode(ID(t[1]), t[2])
    else:
        t[0] = ListNode()
def p_paramlist_mid(t):
    '''param_mid : COMMA VAR param_mid
    |'''
    if len(t) == 4:
        t[0] = ListNode(ID(t[2]), t[3])
    else:
        t[0] = ListNode()


def p_expression_function(t):                                       # CALL
    'expression : VAR LPAREN list_head RPAREN'
    t[0] = FunctionCallNode(ID(t[1]), t[3])




def p_statement_if(t):
    'statement : IF LPAREN expression RPAREN statement elif'
    t[0] = IfElNode(t[3], t[5], t[6])


def p_statement_elif(t):
    '''elif : ELIF LPAREN expression RPAREN statement elif
    | ELSE statement
    |'''
    if len(t) == 7 :
        t[0] = IfElNode(t[3], t[5], t[6])
    elif len(t) == 3:
        t[0] = ElseNode(t[2])
    else:
        t[0] = ElseNode()   # Empty Node, with empty exec


def p_statement_while(t):
    'statement : WHILE LPAREN expression RPAREN statement'
    t[0] = WhileNode(t[3], t[5])

#'''
def p_statement_print(t):       ####################### MAKE THIS INTO EXPRESSION AND REMOVE SEMICOLON IF EVAL ON FUNCTION IS REQUIRED, THIS FORM IS FOR GRAMMAR SYNTAX ERROR THRWOING
    'statement : PRINT LPAREN expression RPAREN SEMICOLON'
    t[0] = PrintNode(t[3])


def p_expression_assign(t):
    'statement : expression ASSIGN expression SEMICOLON'
    # t[0] = Assign(t[1], t[3])
    t[0] = LROp(t[1], t[3])


def p_expression_sequence(t):
    'expression : STRING'
    t[0] = Sequence(t[1])


def p_expression_list(t):
    'expression : list'
    t[0] = t[1]


def p_expression_number(t):
    '''expression : INTEGER
    | REAL'''
    t[0] = Number(t[1])


def p_expression_parenthesis(t):
    'expression : LPAREN expression RPAREN'
    t[0] = t[2]


def p_expression_variable(t):
    'expression : VAR'
    t[0] = Variable(ID(t[1]))




# list
def p_expression_list_literal(t):
    'list : LBRACK list_head RBRACK'
    t[0] = t[2]
def p_list_literal_head(t):
    '''list_head : expression list_mid
    | list_mid'''
    if len(t) == 3:
        t[0] = ListNode(t[1], t[2])
    else:
        t[0] = t[1]
def p_list_literal_mid(t):
    '''list_mid : COMMA expression list_mid
    |'''
    if len(t) == 4:
        t[0] = ListNode(t[2], t[3])
    else:
        t[0] = ListNode()



# operations


def p_expression_list_access(t):
    'expression : expression LBRACK expression RBRACK'
    t[0] = ListGet(t[1], t[3])


def p_expression_op_native(t):
    '''expression : expression PLUS expression
    | expression IN expression'''
    t[0] = NativeBinop(t[2], t[1], t[3])


def p_expression_op_int_unary(t):
    '''expression : NOT expression'''
    t[0] = NumberNot(t[2])


def p_expression_op_number(t):
    '''
    expression : expression MINUS expression
        | expression DIVIDE expression
        | expression TIMES expression
        | expression POWER expression
        | expression FLOOR expression
        | expression MOD expression
        | expression GRT expression
        | expression GEQ expression
        | expression LST expression
        | expression LEQ expression
        | expression EQV expression
        | expression NEQ expression
        | expression AND expression
        | expression OR expression'''
    t[0] = NumberBinop(t[2], t[1], t[3])




#'''
import sys

filename = sys.argv[1]
try:
    file = open(filename)

except IOError:
    print("Invalid file")
    exit()

sentences = file.read().strip()#.split('\n')

#yacc.yacc(debug=0, write_tables=0)

parser = yacc.yacc(debug=0, write_tables=0)
#parser = yacc.yacc()
try:
    s = parser.parse(sentences)
    s.exec()
except EOFError:
    pass
except ScriptSyntaxError as ae:
    print("SYNTAX ERROR")
    #print("--DEBUG (catch): "+str(ae))
except Exception as e:
    print("SEMANTIC ERROR")
    #print("--DEBUG (catch): " + str(e))

# print("FUNCTION_LIST:::")
# print(function_list)
'''
for i in sentences:
    try:
        s = parser.parse(i)
    except ScriptSyntaxError:
        print("SYNTAX ERROR")
    except EOFError:
        break
    except Exception:
        print("SEMANTIC ERROR")
'''
#exit()
#'''

'''
parser = yacc.yacc()
while(1):
    try:
        s = input("INPUT>")
        parser.parse(s)
    except EOFError:
        break
    except ScriptSyntaxError as ae:
        print("SYNTAX ERROR")
        print("--DEBUG (catch): "+str(ae))
    except Exception as e:
        print("SEMANTIC ERROR")
        print("--DEBUG (catch): " + str(e))

'''