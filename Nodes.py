# variables = {}

class Global:
    def __init__(self):
        self.return_val = None
        self.continue_val = True

glob = Global()
function_list = {}

class Scope:
    def __init__(self, parent=None):
        self.parent = parent
        self.variables = {}

    def find_existing_scope(self, var_name):
        current_scope = self
        while current_scope is not None:
            if var_name in current_scope.variables:
                return current_scope
            current_scope = current_scope.parent
        return None

    def assign(self, var_name, value):
        target_scope = self.find_existing_scope(var_name)
        if target_scope is None:
            target_scope = self
        target_scope.variables[var_name] = value

    def add(self, var_name, value):
        self.variables[var_name] = value

    def get(self, var_name):
        target_scope = self.find_existing_scope(var_name)
        if target_scope is None:
            return None
        else:
            return target_scope.variables[var_name]


root_scope = Scope()
main_method_scope = Scope(root_scope)
scope_stack = [root_scope, main_method_scope]

class ScriptSyntaxError(Exception):
    pass

def number(num):
    if not (type(num) == int or type(num) == float):
        raise TypeError("DEBUG: Expected Numbers, but got: ", type(num))
    else:
        return num

def same_type(a, b):
    typeA = type(a)
    typeB = type(b)
    if typeA == int or typeA == float:
        if typeB == int or typeB == float:
            return 1
    return type(a) == type(b)

class Node:
    def eval(self):
        raise ScriptSyntaxError("From Node.eval(), NOT EVALUABLE (FOR NOW?): " + str(type(self)))
        #return 0

    def exec(self):
        raise ScriptSyntaxError("From Node.exec(), NOT EXECUTABLE: " + str(type(self)))
        #return 0


class PrintNode(Node):
    def __init__(self, value):
        self.value = value

    def exec(self):
        val = self.value.eval()
        if type(val) == str:
            print('\'' + val + '\'')
        else:
            print(self.value.eval())


class Statement(Node):
    def __init__(self, stmt):
        self.stmt = stmt

    def exec(self):
        self.stmt.exec()



class Number(Node):
    def __init__(self, number):
        self.number = number

    def eval(self):
        return self.number


class Sequence(Node):
    def __init__(self, sequence):
        self.sequence = sequence

    def eval(self):
        return self.sequence


class NumberNot(Node):
    def __init__(self, number):
        self.number = number

    def eval(self):
        return int(not number(self.number.eval()))


class NumberBinop(Node):
    def __init__(self, op, v1, v2):
        self.v1 = v1
        self.v2 = v2
        self.op = op

    def eval(self):
        v1 = number(self.v1.eval())
        v2 = number(self.v2.eval())
        if self.op == '-': return v1 - v2
        if self.op == '*': return v1 * v2
        if self.op == '/': return v1 / v2
        if self.op == '**': return v1 ** v2
        if self.op == '//': return v1 // v2
        if self.op == '%': return v1 % v2
        if self.op == '<': return int(v1 < v2)
        if self.op == '<=': return int(v1 <= v2)
        if self.op == '>': return int(v1 > v2)
        if self.op == '>=': return int(v1 >= v2)
        if self.op == '<>': return int(v1 != v2)
        if self.op == '==': return int(v1 == v2)
        if self.op == 'and': return int(v1 != 0 and v2 != 0)
        if self.op == 'or': return int(not (v1 == 0 and v2 == 0))
        print("DEBUG: OP NOT FOUND AT NUMBER BINOP: ", self.op, v1, v2)


class NativeBinop(Node):
    def __init__(self, op, v1, v2):
        self.v1 = v1
        self.v2 = v2
        self.op = op

    def eval(self):
        v1 = self.v1.eval()
        v2 = self.v2.eval()
        if self.op == '+': return v1 + v2
        if self.op == 'in': return int(v1 in v2)
        print("DEBUG: OP NOT FOUND AT NATIVE BINOP: ", self.op, v1, v2)


class ListNode(Node):
    def __init__(self, elem=None, sub=None):
        self.element = elem
        self.sublist = sub

    def eval(self):
        if self.element is None and self.sublist is None:
            return []
        elif self.sublist is None:
            return [self.element.eval()]
        else:
            l = self.sublist.eval()
            l.insert(0, self.element.eval())
            return l


class LROp(Node):
    def __init__(self, lvalue, rvalue):
        self.lvalue = lvalue
        self.rvalue = rvalue

    def eval(self): # print( x = 1 )
        self.exec()
        return self.lvalue.eval()

    def exec(self):
        self.lvalue.lexec(self.rvalue)      # rvalue is not evaluated


class LValue:
    def lexec(self, rvalue):
        pass


class ListGet(Node, LValue):
    def __init__(self, list, index):
        self.list = list
        self.index = index

    def eval(self):
        lst = self.list.eval()
        ind = self.index.eval()
        return lst[ind]

    def lexec(self, rvalue):
        lst = self.list.eval()
        ind = self.index.eval()
        lst[ind] = rvalue.eval()


class ID(Node):
    def __init__(self, id):
        self.id = id

    def eval(self):
        return self.id


class Variable(Node, LValue):   # name is a NODE, VARIABLE IS A WRAPPER FOR ANY NODE EVALUATING TO A VAR
    def __init__(self, name):############################################################################################
        self.name = name

    def eval(self):
        #val = variables[self.name.eval()]
        val = scope_stack[-1].get(self.name.eval())
        if val is None:
            # print("DEBUG: variable is null: " + self.name)
            raise ValueError("variable is null: " + self.name)
        return val

    def lexec(self, rvalue):
        #variables[self.name.eval()] = rvalue.eval()
        scope_stack[-1].assign(self.name.eval(), rvalue.eval())


class StatementList(Node):
    def __init__(self, statementhead):
        self.statementhead = statementhead

    def exec(self):
        # new scope, set parent to TOP, push # OR NOT, if the  {} does not create scope
        self.statementhead.exec()
        # pop scope






class FunctionCallNode(Node):
    def __init__(self, name, param_values): # idNode, listNode
        self.name = name
        self.param_values = param_values

    def eval(self):
        functionNode = function_list[self.name.eval()]

        new_scope = Scope(scope_stack[0])

        functionVars = functionNode.param_vars.eval()
        functionValues = self.param_values.eval()
        if len(functionVars) != len(functionValues):
            raise TypeError("Parameter length not equal")
        for var, val in zip(functionVars, functionValues):
            new_scope.add(var, val)

        scope_stack.append(new_scope)

        functionNode.stmtlist.exec()

        glob.continue_val = True
        ret = glob.return_val
        glob.return_val = None

        scope_stack.pop()

        if ret is None:
            raise TypeError("No return value")

        return ret

        # new scope + set PARENT to bottom of stack
        # add param.eval() to scope


        # find function by name
        # exec funct.stmtlits.exec()              # b/c we pushed our unique scope on top, stmtlist will refer to that scope now
        # check ret
        # reset global ret

        # pop scope


class ReturnNode(Node):
    def __init__(self, expr):
        self.expr = expr

    def exec(self):
        glob.return_val = self.expr.eval()
        glob.continue_val = False


class FunctionDeclareNode(Node):
    def __init__(self, id, param_vars, stmtlist):   # node, listnode of ID nodes, node
        self.id = id
        self.param_vars = param_vars
        self.stmtlist = stmtlist

    def exec(self):
        function_list[self.id.eval()] = self








class StatementHead(Node):
    def __init__(self, statement=None, tails=None):
        self.statement = statement
        self.tails = tails

    def exec(self):                     # always have an empty tail, but it will not break anything cuz Null check
        if self.statement is not None:
            self.statement.exec()
            if glob.continue_val:
                self.tails.exec()


class IfElNode(Node):
    def __init__(self, expr, block, elifnode):
        self.expr = expr
        self.block = block
        self.elifnode = elifnode

    def exec(self):
        if self.expr.eval():
            self.block.exec()
        else:
            self.elifnode.exec()


class ElseNode(Node):
    def __init__(self, block=None):
        self.block = block

    def exec(self):
        if self.block is not None:
            self.block.exec()

class WhileNode(Node):
    def __init__(self, expr, block):
        self.expr = expr
        self.block = block

    def exec(self):
        while self.expr.eval():
            self.block.exec()

